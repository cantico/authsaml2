<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
*/


require_once dirname(__FILE__).'/functions.php';

/**
 * Insert service provider links in sitemap
 */
function AuthSaml2_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event) {
    
    bab_functionality::includeOriginal('Icons');
    
    $item = $event->createItem('AuthSaml2_ServiceProvider');
    $item->setPosition(array('root', 'DGAll'));
    $item->setLabel(AuthSaml2_translate('SAML2 Service provider'));
    $item->addIconClassname(Func_Icons::ACTIONS_AUTHENTICATE);
    $event->addFolder($item);
    
    $item = $event->createItem('AuthSaml2_SingleLogoutServiceLocation');
    $item->setPosition(array('root', 'DGAll', 'AuthSaml2_ServiceProvider'));
    $item->setLabel('SingleLogoutService Location');
    $item->setLink('?tg=addon/authsaml2/logout');
    $item->addIconClassname(Func_Icons::ACTIONS_LOGOUT);
    $event->addFunction($item);
    
    $item = $event->createItem('AuthSaml2_SingleLogoutServiceResponseLocation');
    $item->setPosition(array('root', 'DGAll', 'AuthSaml2_ServiceProvider'));
    $item->setLabel('SingleLogoutService ResponseLocation');
    $item->setLink('?tg=addon/authsaml2/logoutresponse');
    $item->addIconClassname(Func_Icons::ACTIONS_LOGOUT);
    $event->addFunction($item);
    
    $item = $event->createItem('AuthSaml2_AssertionConsumerServiceLocation');
    $item->setPosition(array('root', 'DGAll', 'AuthSaml2_ServiceProvider'));
    $item->setLabel('AssertionConsumerService Location');
    $item->setLink('?tg=addon/authsaml2/acs');
    $item->addIconClassname(Func_Icons::ACTIONS_LOGIN);
    $event->addFunction($item);
}