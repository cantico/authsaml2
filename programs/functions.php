<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
*/

require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

function AuthSaml2_translate($str)
{
	return $str;
}

/**
 * @return bab_Registry
 */
function AuthSaml2_getRegistry()
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/AuthSaml2/');
    
    return $registry;
}


/**
 * @return bab_Path
 */
function AuthSaml2_getFile($folderName)
{
    $addon = bab_getAddonInfosInstance('AuthSaml2');
    $basePath = new bab_Path($addon->getUploadPath(), $folderName);
    
    foreach ($basePath as $file) {
        return $file;
    }
    
    return null;
}

/**
 * Authenticate the SAML user in Ovidentia session
 * @param SamlUser $user
 */
function AuthSaml2_authenticateInOvidentia(SamlUser $user)
{
    require_once $GLOBALS['babInstallPath'].'utilit/userinfosincl.php';
    
    $emails = $user->getAttribute('mail');
    foreach ($emails as $email) {
        $id_user = bab_getUserIdByEmail($email);
        if (0 !== $id_user) {
            break;
        }
    }
     
    if (0 === $id_user) {
        throw new Exception(sprintf(AuthSaml2_translate('The email %s from SAML2 server does not exists in Ovidentia'), $email));
    }
     
    if (!bab_userInfos::isValid($id_user)) {
        throw new Exception(sprintf(AuthSaml2_translate('The account %s is not valid in Ovidentia'), $email));
    }
    
    $auth = bab_functionality::get('PortalAuthentication/AuthOvidentia');
    /*@var $auth Func_PortalAuthentication_AuthOvidentia */
    $auth->setUserSession($id_user);
    
    $session = bab_getInstance('bab_Session');
    /*@var $session bab_Session */
    
    $session->sAuthPath = 'PortalAuthentication/AuthSaml2';
    
    bab_debug('Authenticate Ovidentia session from SAML2 user', DBG_INFO, 'AuthSaml2');
}