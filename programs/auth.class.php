<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


bab_functionality::includeFile('PortalAuthentication');




class Func_PortalAuthentication_AuthSaml2 extends Func_PortalAuthentication
{

	public function getDescription() 
	{
		return AuthSaml2_translate("SAML2 Authentication");
	}
	
	
	/**
	 * If authentication use ovidentia database
	 * This is used by the emailpassword functionality and administrator users list
	 * @var string
	 */
	public function getLoginIdField() {
	    return 'email';
	}
	
	
	/**
	 * Use a configurable menu name
	 */
	public function getMenuName()
	{
	    $registry = AuthSaml2_getRegistry();
	    if ($serverName = $registry->getValue('serverName')) {
	        return $serverName;
	    }
	    
	    return parent::getMenuName();
	}
	
	
	/**
	 * @return Auth_Source_SP
	 */
	protected function getAuthSource()
	{
	    require_once dirname (__FILE__) . '/saml/include.php';
	    require_once dirname (__FILE__) . '/saml/compatibility.php';
	    
	    
	    $config = KitSamlConfig::getInstance();
	    $container = new KitSamlContainer();
	    SAML2_Compat_ContainerSingleton::setContainer($container);
	    
	    return $config->getAuthSource();
	}
	

	
	
	public function login()
	{
	    if ($this->isLogged()) {
	        return true;
	    }
	    
	    // this will redirect
		$this->getAuthSource()->authenticate();
		return true;
	}

	
	public function logout()
	{
	    if (!$this->isLogged()) {
	        return true;
	    }
	    
	    $registry = AuthSaml2_getRegistry();
	    if ($registry->getValue('samlLogout')) {
	        // we logout from ovidentia in logout.php
	        $this->getAuthSource()->logout();
	    } else {
	        // logout and redirect
	        bab_logout();
	    }
	    return true;
	}
}






