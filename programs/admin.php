<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';



class AuthSaml2_ConfigurationPage
{

    private function addSeviceProviderUrl($form, $name)
    {
        $W = bab_Widgets();
        $sitemap = bab_siteMap::getFromSite();
        $nodeId = 'AuthSaml2_'.$name;
        
        $node = $sitemap->getNodeByTargetId('Custom', $nodeId);
        if (!isset($node)) {
            $node = $sitemap->getNodeById($nodeId);
        }
        
        if (!isset($node)) {
            $form->addItem($W->Label(sprintf(AuthSaml2_translate('Url for %s not found in sitemap'), $name)));
            return;
        }
        
        $sitemapItem = $node->getData();
        /*@var $sitemapItem bab_siteMapItem */
        
        $form->addItem($W->Label(sprintf('%s: %s', $name, $GLOBALS['babUrl'].$sitemapItem->getRwUrl())));
        
        if (!$sitemapItem->rewritingEnabled()) {
            $form->addItem($W->Icon(
                AuthSaml2_translate('The rewriting must be activated to have a valid URL'), 
                Func_Icons::STATUS_DIALOG_ERROR
            ));
        }
    }

	private function getForm()
	{
	    bab_functionality::includeOriginal('Icons');
		$W = bab_Widgets();
		$addon = bab_getAddonInfosInstance('AuthSaml2');
		$basePath = new bab_Path($addon->getUploadPath());
		
		$form = $W->Form();
		$form->setName('configuration')
			->addClass('BabLoginMenuBackground')
			->addClass('widget-bordered')
			->addClass('widget-centered')
		    ->addClass(Func_Icons::ICON_LEFT_24);
		
		$form->setCanvasOptions($form->Options()->width(70,'em'));
		
		$form->setHiddenValue('tg', bab_rp('tg'));
		$form->colon();

		$form->getLayout()->setVerticalSpacing(2,'em');
		
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('Server name'),
		    $W->LineEdit()->addClass('widget-fullwidth'),
		    'serverName',
		    AuthSaml2_translate('Name used in the list of available authentication methods')
		));
		
        
		$idpMetadataPath = clone $basePath;
		$idpMetadataPath->push('idpMetadata');
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('identity provider metadata'),
		    $W->FilePicker()->oneFileMode()->setFolder($idpMetadataPath),
		    'idpMetadata'
		));
		
		$spMetadataPath = clone $basePath;
		$spMetadataPath->push('spMetadata');
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('service provider metadata'),
		    $W->FilePicker()->oneFileMode()->setFolder($spMetadataPath),
		    'spMetadata'
		));
		
		$form->addItem($W->Title(AuthSaml2_translate('Urls for the service provider metadata file:'), 5));
		$this->addSeviceProviderUrl($form, 'SingleLogoutServiceLocation');
		$this->addSeviceProviderUrl($form, 'SingleLogoutServiceResponseLocation');
		$this->addSeviceProviderUrl($form, 'AssertionConsumerServiceLocation');
		
		
		
		$pkcs12KeyPath = clone $basePath;
		$pkcs12KeyPath->push('pkcs12Key');
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('pkcs12 certificate'),
		    $W->FilePicker()->oneFileMode()->setFolder($pkcs12KeyPath),
		    'pkcs12Key'
		));
		
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('Keystore password'),
		    $W->LineEdit()->addClass('widget-fullwidth'),
		    'keystorePassword'
		));
		
		$form->addItem($W->LabelledWidget(
		    AuthSaml2_translate('Logout from SAML2 server when disconnect from Ovidentia'),
		    $W->CheckBox(),
		    'samlLogout'
		));
		
		
		$form->addItem(
				$W->SubmitButton()
				->setLabel(AuthSaml2_translate('Save'))
		);
		
		
		$configuration = array();
		$registry = AuthSaml2_getRegistry();
		while ($key = $registry->fetchChildKey()) {
		    $configuration[$key] = $registry->getValue($key);
		}

		
		$form->setValues(
			array(
				'configuration' => $configuration
			)
		);

		return $form;
	}




	public function display()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();
		

		$page->setTitle(AuthSaml2_translate('Configure SAML2 authentication'));
		
		$page->addItem($this->getForm());
		$page->displayHtml();
	}


	public function save($configuration)
	{
		$registry = AuthSaml2_getRegistry();
		
		$registry->setKeyValue('serverName' , $configuration['serverName']);
		$registry->setKeyValue('keystorePassword' , $configuration['keystorePassword']);
		$registry->setKeyValue('samlLogout' , (bool) $configuration['samlLogout']);

	    
		bab_url::get_request('tg')->location();
	}
}


if (!bab_isUserAdministrator()) {
	return;
}


$page = new AuthSaml2_ConfigurationPage;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();
