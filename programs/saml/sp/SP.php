<?php

// require_once 'State.php';

require_once __DIR__ . '/../include.php';

class Auth_Source_SP {
	
	/**
	 * The authentication source identifier.
	 *
	 * This identifier can be used to look up this object, for example when returning from a login form.
	 */
	protected $authId;

	/**
	 * The entity ID of this SP.
	 *
	 * @var string
	 */
	private $entityId;


	/**
	 * The metadata of this SP.
	 *
	 * @var Metadata.
	 */
	private $metadata;


	/**
	 * The IdP the user is allowed to log into.
	 *
	 * @var string|NULL  The IdP the user can log into, or NULL if the user can log into all IdPs.
	 */
	private $idp;


	/**
	 * URL to discovery service.
	 *
	 * @var string|NULL
	 */
	private $discoURL;


	/**
	 * Constructor for SAML SP authentication source.
	 *
	 * @param array $info  Information about this authentication source.
	 * @param array $config  Configuration.
	 */
	public function __construct(Metadata $metadata, $authId) {
		$this->metadata = $metadata;
		$this->entityId = $this->metadata->getString('entityid');
		$this->idp = $this->metadata->getString('idp', NULL);
		$this->authId = $authId;
	}

	/**
	 * Retrieve the entity id of this SP.
	 *
	 * @return string  The entity id of this SP.
	 */
	public function getEntityId() {

		return $this->entityId;
	}


	/**
	 * Retrieve the metadata of this SP.
	 *
	 * @return Metadata  The metadata of this SP.
	 */
	public function getMetadata() {

		return $this->metadata;

	}

	/**
	 * Function to actually send the authentication request.
	 *
	 * This function does not return.
	 *
	 * @param SAML2_Binding $binding  The binding.
	 * @param SAML2_AuthnRequest  $ar  The authentication request.
	 */
	public function sendSAML2AuthnRequest(SAML2_Binding $binding, SAML2_AuthnRequest $ar) {
		$binding->send($ar);
//sleep(5);		
//assert('FALSE');
	}

	/**
	 * Start login.
	 *
	 * This function saves the information about the login, and redirects to the IdP.
	 *
	 */
	public function authenticate() {
		$config = KitSamlConfig::getInstance();
		$idpMetadata = $config->getIdpMetadata();
		
		$ar = Message::buildAuthnRequest($this->metadata, $idpMetadata);
		
		$defaultAC = null;
		foreach ( $this->metadata->getMetadata()->RoleDescriptor [0]->AssertionConsumerService as $acs ) {
			if ($acs->isDefault) {
				$defaultAC = $acs;
				break;
			}
		}
		$ar->setAssertionConsumerServiceURL($defaultAC->Location);

		$relayState=$config->getLoginRedirection();
		if(empty($relayState)) {
			$relayState = Utilities::getBaseURL();
		} 
		$ar->setRelayState($relayState);
		
		$ar->setIDPList(array_unique(array_merge($this->metadata->getArray('IDPList', array()), 
												$idpMetadata->getArray('IDPList', array()))));

		if ($idpMetadata->getInteger('ProxyCount', null) !== null) {
			$ar->setProxyCount($idpMetadata->getInteger('ProxyCount', null));
		} else if ($this->metadata->getInteger('ProxyCount', null) !== null) {
			$ar->setProxyCount($this->metadata->getInteger('ProxyCount', null));
		}

		KitSamlLogger::debug('Sending SAML 2 AuthnRequest to ' . var_export($idpMetadata->getString('entityid'), TRUE));

		/* Select appropriate SSO endpoint */
		if ($ar->getProtocolBinding() === SAML2_Const::BINDING_HOK_SSO) {
			$dst = $idpMetadata->getDefaultEndpoint('SingleSignOnService', array(
				SAML2_Const::BINDING_HOK_SSO)
			);
		} else {
			$dst = $idpMetadata->getDefaultEndpoint('SingleSignOnService', array(
				SAML2_Const::BINDING_HTTP_REDIRECT,
				SAML2_Const::BINDING_HTTP_POST)
			);
		}
		$ar->setDestination($dst['Location']);

		$b = SAML2_Binding::getBinding($dst['Binding']);

		$this->sendSAML2AuthnRequest($b, $ar);
		//assert('FALSE'); /* Should not return. */
	}


	/**
	 * Start logout operation.
	 *
	 */
	public function logout() {
		$session=SamlSession::getSessionFromRequest();
		
		$config = KitSamlConfig::getInstance();
		$idpMetadata = $config->getIdpMetadata();

		$endpoint = $idpMetadata->getEndpointPrioritizedByBinding('SingleLogoutService', array(
			SAML2_Const::BINDING_HTTP_POST,
			SAML2_Const::BINDING_HTTP_POST), FALSE);
		if ($endpoint === FALSE) {
			KitSamlLogger::info('No logout endpoint for IdP.');
			return;
		}

		$lr = Message::buildLogoutRequest($this->metadata, $idpMetadata);
		
		
		$lr->setNameId(array('Value' => $session->getNameId()));
		$lr->setSessionIndex($session->getIdpSessionId());
		
		$lr->setDestination($endpoint['Location']);
		
		$lr->encryptNameId(Message::getEncryptionKey($idpMetadata));
	
		$b = SAML2_Binding::getBinding($endpoint['Binding']);
		
		$b->send($lr);

	}


	/**
	 * Handle a response from a SSO operation.
	 *
	 * @param SAML2_Response $response  Response to handle.
	 */
	/**
	 * 
	 * @param SAML2_Response $response
	 * @param SAML2_Assertion[] $assertions
	 * @throws KitSamlException
	 */
	public function handleResponse(SAML2_Response $response, $assertions) {
		assert('is_array($assertions)');
		
		$authenticatingAuthority = NULL;
		$nameId = NULL;
		$sessionIndex = NULL;
		$expire = NULL;
		$attributes = array();
		$foundAuthnStatement = FALSE;
		foreach ($assertions as $assertion) {
		
			if ($authenticatingAuthority === NULL) {
				$authenticatingAuthority = $assertion->getAuthenticatingAuthority();
			}
			if ($nameId === NULL) {
				$nameId = $assertion->getNameId();
			}
		
			if ($sessionIndex === NULL) {
				$sessionIndex = $assertion->getSessionIndex();
			}
			if ($expire === NULL) {
				$expire = $assertion->getSessionNotOnOrAfter();
			}
		
			$attributes = array_merge($attributes, $assertion->getAttributes());
		
			if ($assertion->getAuthnInstant() !== NULL) {
				/* Assertion contains AuthnStatement, since AuthnInstant is a required attribute. */
				$foundAuthnStatement = TRUE;
			}
		}
		
		if (!$foundAuthnStatement) {
			throw new KitSamlException('No AuthnStatement found in assertion(s).');
		}
		
		
		// Save user in session.
		$user = new SamlUser($assertion->getAttributes());
		
		
		$session=SamlSession::getSessionFromRequest();
		$session->setUser($user);
		$session->setIdpSessionId($sessionIndex);
		$session->setNameId($sessionIndex);
		SamlSessionHandler::getSessionHandler()->saveSession($session);
		
		$relayState=$response->getRelayState();
		if(empty($relayState)) {
			$redirectTo=KitSamlConfig::getInstance()->getLoginRedirection();
		} else {
			$redirectTo=$relayState;
		}
		
		KitSamlLogger::debug('redirect : '. $redirectTo);
		
		AuthSaml2_authenticateInOvidentia($user);
		
		// redirect
		Utilities::redirectUntrustedURL(urldecode ($redirectTo));
		
		
	}


}
