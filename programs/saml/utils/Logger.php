<?php

/**
 * The main logger class
 */

class KitSamlLogger
{
    private static $loggingHandler = NULL;
    private static $logLevel = NULL;
    private static $captureLog = FALSE;
    private static $capturedLog = array();

    /**
     * Array with messages logged before the logging handler was initialized.
     *
     * @var array
     */
    private static $earlyLog = array();


    /**
     * This constant defines the string we set the track ID to while we are fetching the track ID from the session
     * class. This is used to prevent infinite recursion.
     */
    private static $TRACKID_FETCHING = '_NOTRACKIDYET_';

    /**
     * This variable holds the track ID we have retrieved from the session class. It can also be NULL, in which case
     * we haven't fetched the track ID yet, or TRACKID_FETCHING, which means that we are fetching the track ID now.
     */
    private static $trackid = NULL;

    /**
     * This variable holds the format used to log any message. Its use varies depending on the log handler used (for
     * instance, you cannot control here how dates are displayed when using syslog or errorlog handlers), but in
     * general the options are:
     *
     * - %date{<format>}: the date and time, with its format specified inside the brackets. See the PHP documentation
     *   of the strftime() function for more information on the format. If the brackets are omitted, the standard
     *   format is applied. This can be useful if you just want to control the placement of the date, but don't care
     *   about the format.
     *
     * - %process: kitsaml
     *
     * - %level: the log level (name or number depending on the handler used).
     *
     * - %stat: if the log entry is intended for statistical purposes, it will print the string 'STAT ' (bear in mind
     *   the trailing space).
     *
     *
     * - %srcip: the IP address of the client. If you are behind a proxy, make sure to modify the
     *   $_SERVER['REMOTE_ADDR'] variable on your code accordingly to the X-Forwarded-For header.
     *
     * - %msg: the message to be logged.
     *
     * @var string The format of the log line.
     */
    private static $format = '%date{%b %d %H:%M:%S} %process %level %stat %msg';

    const EMERG = 0;
    const ALERT = 1;
    const CRIT = 2;
    const ERR = 3;
    const WARNING = 4;
    const NOTICE = 5;
    const INFO = 6;
    const DEBUG = 7;


    /**
     * Log an emergency message.
     *
     * @var string $string The message to log.
     */
    public static function emergency($string)
    {
        self::log(self::EMERG, $string);
    }


    /**
     * Log a critical message.
     *
     * @var string $string The message to log.
     */
    public static function critical($string)
    {
        self::log(self::CRIT, $string);
    }


    /**
     * Log an alert.
     *
     * @var string $string The message to log.
     */
    public static function alert($string)
    {
        self::log(self::ALERT, $string);
    }


    /**
     * Log an error.
     *
     * @var string $string The message to log.
     */
    public static function error($string)
    {
        bab_debug($string, DBG_ERROR, 'AuthSaml2');
        self::log(self::ERR, $string);
    }


    /**
     * Log a warning.
     *
     * @var string $string The message to log.
     */
    public static function warning($string)
    {
        bab_debug($string, DBG_WARNING, 'AuthSaml2');
        self::log(self::WARNING, $string);
    }

    /**
     * We reserve the notice level for statistics, so do not use this level for other kind of log messages.
     *
     * @var string $string The message to log.
     */
    public static function notice($string)
    {
        bab_debug($string, DBG_TRACE, 'AuthSaml2');
        self::log(self::NOTICE, $string);
    }


    /**
     * Info messages are a bit less verbose than debug messages. This is useful to trace a session.
     *
     * @var string $string The message to log.
     */
    public static function info($string)
    {
        bab_debug($string, DBG_INFO, 'AuthSaml2');
        self::log(self::INFO, $string);
    }


    /**
     * Debug messages are very verbose, and will contain more information than what is necessary for a production
     * system.
     *
     * @var string $string The message to log.
     */
    public static function debug($string)
    {
        bab_debug($string, DBG_DEBUG, 'AuthSaml2');
        self::log(self::DEBUG, $string);
    }


    /**
     * Statistics.
     *
     * @var string $string The message to log.
     */
    public static function stats($string)
    {
        bab_debug($string, DBG_INFO, 'AuthSaml2');
        self::log(self::NOTICE, $string, TRUE);
    }


    /**
     * Set the logger to capture logs.
     *
     * @var boolean $val Whether to capture logs or not. Defaults to TRUE.
     */
    public static function setCaptureLog($val = TRUE)
    {
        self::$captureLog = $val;
    }


    /**
     * Get the captured log.
     */
    public static function getCapturedLog()
    {
        return self::$capturedLog;
    }


    private static function createLoggingHandler()
    {
        // set to FALSE to indicate that it is being initialized
        self::$loggingHandler = FALSE;

        // get the configuration
        $config = KitSamlConfig::getInstance();
        
        $sh = new LoggingHandlerFile();

        // setting minimum log_level
        LoggingHandlerFile::$levelNames;
        $index = 0;
        foreach (LoggingHandlerFile::$levelNames as $levelName) {
        	if($config->getLogLevel() === $levelName) {
        		self::$logLevel = $index;
        		break;
        	}
        	$index++;
        }
        
        if($index > KitSamlLogger::DEBUG) {
        	throw new Exception( "LogLevel: " . $config->getLogLevel() ." unknown." );
        }
        

//         self::$format = $config->getloString('logging.format', self::$format);
        $sh->setLogFormat(self::$format);

        // set the session handler
        self::$loggingHandler = $sh;
    }


    private static function log($level, $string, $statsLog = FALSE)
    {
    	
        if (self::$loggingHandler === NULL) {
            /* Initialize logging. */
            self::createLoggingHandler();

            if (!empty(self::$earlyLog)) {
                error_log('----------------------------------------------------------------------');
                // output messages which were logged before we properly initialized logging
                foreach (self::$earlyLog as $msg) {
                    self::log($msg['level'], $msg['string'], $msg['statsLog']);
                }
            }
        } elseif (self::$loggingHandler === FALSE) {
            // some error occurred while initializing logging
            if (empty(self::$earlyLog)) {
                // this is the first message
                error_log('--- Log message(s) while initializing logging ------------------------');
            }
            error_log($string);

            self::$earlyLog[] = array('level' => $level, 'string' => $string, 'statsLog' => $statsLog);
            return;
        }

        if (self::$captureLog) {
        	print "tete\n";
            $ts = microtime(TRUE);
            $msecs = (int) (($ts - (int) $ts) * 1000);
            $ts = GMdate('H:i:s', $ts).sprintf('.%03d', $msecs).'Z';
            self::$capturedLog[] = $ts.' '.$string;
        }

        if (self::$logLevel >= $level || $statsLog) {
            if (is_array($string)) {
                $string = implode(",", $string);
            }

            $formats = array('%trackid', '%msg', '%srcip', '%stat');
            $replacements = array("", $string, ""/*$_SERVER['REMOTE_ADDR']*/);

            $stat = '';
            if ($statsLog) {
                $stat = 'STAT ';
            }
            array_push($replacements, $stat);

            $string = str_replace($formats, $replacements, self::$format);
            self::$loggingHandler->log($level, $string);
        }
    }

}


/**
 * A class for logging in a file
 *
 */

class LoggingHandlerFile {
    /**
     * This array contains the mappings from syslog loglevel to names.
     */
    public static $levelNames = array(
        KitSamlLogger::EMERG   => 'EMERGENCY',
        KitSamlLogger::ALERT   => 'ALERT',
        KitSamlLogger::CRIT    => 'CRITICAL',
        KitSamlLogger::ERR     => 'ERROR',
        KitSamlLogger::WARNING => 'WARNING',
        KitSamlLogger::NOTICE  => 'NOTICE',
        KitSamlLogger::INFO    => 'INFO',
        KitSamlLogger::DEBUG   => 'DEBUG',
    );
    private $logFile = NULL;
    private $format;


    /**
     * Build a new logging handler based on files.
     */
    public function __construct()
    {
        $config = KitSamlConfig::getInstance();

        // get the metadata handler option from the configuration
        $this->logFile = $config->getLogFileName();
        
        if (null === $this->logFile) {
            return;
        }

        if (@file_exists($this->logFile)) {
            if (!@is_writeable($this->logFile)) {
                throw new Exception("Could not write to logfile: " . $this->logFile);
            }
        } else {
            if (!@touch($this->logFile)) {
                throw new Exception(
                    "Could not create logfile: " . $this->logFile .
                    " Loggingdir is not writeable for the webserver user."
                );
            }
        }
        
    }


    /**
     * Set the format desired for the logs.
     *
     * @param string $format The format used for logs.
     */
    public function setLogFormat($format)
    {
        $this->format = $format;
    }


    /**
     * Log a message to the log file.
     *
     * @param int $level The log level.
     * @param string $string The formatted message to log.
     */
    public function log($level, $string)
    {
        if ($this->logFile != NULL) {
            // set human-readable log level.
            $levelName = sprintf('UNKNOWN%d', $level);
            if (array_key_exists($level, self::$levelNames)) {
                $levelName = self::$levelNames[$level];
            }

            $formats = array('%process', '%level');
            $replacements = array("kitsaml", $levelName);
            
            $matches = array();
            if (preg_match('/%date(?:\{([^\}]+)\})?/', $this->format, $matches)) {
                $format = "%b %d %H:%M:%S";
                if (isset($matches[1])) {
                    $format = $matches[1];
                }

                array_push($formats, $matches[0]);
                array_push($replacements, strftime($format));
            }

            $string = str_replace($formats, $replacements, $string);
            file_put_contents($this->logFile, $string.PHP_EOL, FILE_APPEND);
        }
    }
}

