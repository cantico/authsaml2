<?php

require_once 'include.php';

class KitSamlConfig {
	
	private $config;
	
	private $spMetadata;
	
	private $idpMetadata;
	
	private $authSource;
	
	private static $instance;
	
	private static $isConfigured = FALSE;
		
	private function __construct() {
	    
	    
	    $idpMetadata = AuthSaml2_getFile('idpMetadata');
	    $spMetadata = AuthSaml2_getFile('spMetadata');
		
		// Load Metadatas
		$document = new DOMDocument();
			
		if (isset( $idpMetadata )) {
			$document->load ( $idpMetadata->tostring() );
			$this->idpMetadata = new Metadata($document->documentElement, FALSE);
		}
		

		if(isset ( $spMetadata )) {
			$document->load ( $spMetadata->tostring() );
			$this->spMetadata = new Metadata($document->documentElement, TRUE);
			$this->authSource = new Auth_Source_SP($this->spMetadata, 'authSAML');
		}
		
		self::$isConfigured = (isset($idpMetadata) && isset($spMetadata));
	}
	
	/**
	 * Get a configuration file.
	 *
	 *
	 * @return KitSamlConfig  The configuration object.
	 */
	public static function getInstance() {
		if(!isset(self::$instance) || !self::$isConfigured) {
			self::$instance = new KitSamlConfig();
			
			if (!self::$isConfigured) {
			    throw new Exception('Not configured');
			}
		}
		return self::$instance;
	}
	
	public function getSpFileName() {
	    $spMetadata = AuthSaml2_getFile('spMetadata');
	    return $spMetadata->tostring();
	}
	
	public function getLogLevel() {
		return 'DEBUG';
	}
	
	public function getLogFileName() {
		return null;
	}
	
	public function getLogPattern() {
		return '';
	}
		
	public function getCertPassword() {
	    return AuthSaml2_getRegistry()->getValue('keystorePassword');
	}
	
	public function getCertFile() {
	    $pkcs12Key = AuthSaml2_getFile('pkcs12Key');
		return $pkcs12Key->tostring();
	}
	
	/**
	 * 
	 * @return Metadata
	 */
	public function getSpMetadata() {
		return $this->spMetadata;
	}
	
	/**
	 * 
	 * @return Metadata
	 */
	public function getIdpMetadata() {
		return $this->idpMetadata;
	}
	
	/**
	 * 
	 */
	public function isAttributeEncoded() {
		return false;
	}
	
	public function getAuthSource() {
		return $this->authSource;
	}
	
	public function getLoginRedirection() {
		return $GLOBALS['babUrlScript'] . '?babHttpContext=restore';
	}
	
	public function getLogoutRedirection() {
		return $GLOBALS['babUrl'];
	}
	
	/**
	 * @return string
	 */
	public function getBaseURL() {
	    preg_match('/^http[s]?:\/\/[^\/]+(\/.*)$/', $GLOBALS['babUrl'], $m);
		return $m[1];
	}
	
}