<?php

/**
 * Assertion consumer service handler for SAML 2.0 SP authentication client.
 */

require_once 'include.php';

if(session_id() === '') {
	session_start();
}

// Implement the Container interface (out of scope for example)
$container = new KitSamlContainer();
SAML2_Compat_ContainerSingleton::setContainer($container);

$config = KitSamlConfig::getInstance();

$source = KitSamlConfig::getInstance()->getAuthSource();
$spMetadata = $config->getSpMetadata();

$b = SAML2_Binding::getCurrentBinding();
try {
	$response = $b->receive();
	if (!($response instanceof SAML2_Response)) {
		throw new Exception('Invalid message received to AssertionConsumerService endpoint.');
	}
	
	$idp = $response->getIssuer();
	if ($idp === NULL) {
		/* No Issuer in the response. Look for an unencrypted assertion with an issuer. */
		foreach ($response->getAssertions() as $a) {
			if ($a instanceof SAML2_Assertion) {
				/* We found an unencrypted assertion - there should be an issuer here. */
				$idp = $a->getIssuer();
				break;
			}
		}
		if ($idp === NULL) {
			/* No issuer found in the assertions. */
			throw new Exception('Missing <saml:Issuer> in message delivered to AssertionConsumerService.');
		}
	}
	
	$idpMetadata = $config->getIdPmetadata();
	
	$stateId = $response->getInResponseTo();
	if (!empty($stateId)) {
		/* Check that the issuer is the one we are expecting. */
		if ($idpMetadata->getMetadata()->entityID !== $idp) {
			throw new Exception('The issuer of the response does not match to the identity provider we sent the request to.');
		}
		// TODO verify inResponseTo
		
		
	} else {
		
	}
	
	KitSamlLogger::debug('Received SAML2 Response from ' . var_export($idp, TRUE) . '.');


	$assertions = Message::processResponse($spMetadata->getMetadata(), $idpMetadata->getMetadata(), $response);
	$source->handleResponse($response, $assertions);
	
	
} catch (Exception $e) {
    $babBody = bab_getBody();
    $babBody->addError(bab_getStringAccordingToDataBase($e->getMessage(), 'ISO-8859-1'));
	$babBody->babpopup('');
} 

assert('FALSE');


