<?php

$dirname = dirname(__FILE__);

require_once "{$dirname}/../vendor/autoload.php";
require_once "{$dirname}/metadata/Metadata.php";
require_once "{$dirname}/utils/Utils.php";
require_once "{$dirname}/metadata/SAMLParser.php";
require_once "{$dirname}/exception/KitSamlException.php";
require_once "{$dirname}/exception/ResponseException.php";
require_once "{$dirname}/utils/Message.php";

require_once "{$dirname}/utils/Logger.php";
require_once "{$dirname}/Container.php";
require_once "{$dirname}/Config.php";
require_once "{$dirname}/sp/SP.php";
require_once "{$dirname}/sp/User.php";
require_once "{$dirname}/session/Session.php";
